#include <iostream>
#include <iomanip>
#include <random>
#include <vector>
#include <memory>
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>

using namespace std;

struct Tile {
	bool flagged;
	bool mine;
	bool revealed;
	int x;
	int y;
};

class Board {
private:
	vector<vector<Tile>> _board;
	int mineCount;
	random_device rd;
	mt19937 gen;
	bernoulli_distribution dist;

public:

	Board(int Size, float chance)
	{
		gen = mt19937(rd());
		dist = bernoulli_distribution(chance);
		mineCount = 0;
		for (int i = 0; i < Size; i++)
		{
			_board.push_back(vector<Tile>());
			for (int j = 0; j < Size; j++)
			{
				_board[i].push_back(Tile());
				if (dist(gen))
				{
					_board[i][j].mine = true;
					mineCount++;
				}
				else
				{
					_board[i][j].mine = false;
				}
				_board[i][j].flagged = false;
				_board[i][j].revealed = false;
				_board[i][j].x = i;
				_board[i][j].y = j;
			}
		}
		cout << "Number of mines: " << mineCount << endl;
	}

	bool posValid(int x, int y)
	{
		return !(x < 0 || y < 0 || x > _board.size() - 1 || y > _board.size() - 1);
	}

	vector<Tile*> getAdjacentTiles(Tile * t)
	{
		vector<Tile*> tiles;
		for (int xP = t->x - 1; xP <= t->x + 1; xP++)
		{
			for (int yP = t->y - 1; yP <= t->y + 1; yP++)
			{
				if (!(xP == t->x && yP == t->y) && posValid(xP, yP))
					tiles.push_back(&_board[xP][yP]);
			}
		}
		return move(tiles);
	}
	
	int getAdjacentMines(Tile * t)
	{
		vector<Tile*> v = getAdjacentTiles(t);
		return count_if(v.begin(), v.end(), [](auto t) {return t->mine;});
	}

	char drawTile(Tile & t, bool reveal) 
	{
		if (reveal && t.mine)
			return '*';
		if (t.flagged)
			return 'F';
		if (t.revealed)
		{
			if (t.mine)
				return '*';
			int count = getAdjacentMines(&t);
			if (count)
				return count + '0';
			else
				return ' ';
		}
		return '#';
	}

	void revealRecurse(Tile * t)
	{
		if (!t->mine)
		{
			t->revealed = true;
			t->flagged = false;
			vector<Tile*> adj = getAdjacentTiles(t);
			if (none_of(adj.begin(), adj.end(), [](Tile* t) {return t->mine;}))
				for (Tile * x : adj)
					if (!x->revealed)
						revealRecurse(x);
		}
	}

	int checkWin()
	{
		for (auto v : _board)
			if (any_of(v.begin(), v.end(), [](Tile t) {return t.revealed && t.mine;}))
				return -1; // Loss
		if (all_of(_board.begin(), _board.end(), [](auto v) {
			return none_of(v.begin(), v.end(), [](auto t) {return !t.revealed && !t.mine;});}))
			return 1; // Game Won
		else
			return 0; // Normal

	}

	void revealTile(int x, int y)
	{
		if (posValid(x, y))
		{
			_board[y][x].revealed = true;
			revealRecurse(&_board[y][x]);
			drawTextBased();
		}
		else
		{
			cout << "Invalid Tile" << endl;
		}
	}

	void setFlagged(int x, int y, bool flag)
	{
		if (posValid(x, y))
		{
			if (!_board[y][x].revealed)
			{
				_board[y][x].flagged = flag;
				drawTextBased();
			}
			else
				cout << "Tile already revealed" << endl;
		}
		else
			cout << "Invalid Position" << endl;

	}

	void drawTextBased(bool reveal = false) {
		cout << "  ";
		for (int i = 0; i < _board.size(); i++)
		{
			cout << setw(3) << i;
		}
		cout << endl << endl;
		for (int i = 0; i < _board.size(); i++)
		{
			cout << setw(2) << i << ": ";
			for (int j = 0; j < _board.size(); j++)
			{
				cout << drawTile(_board[i][j], reveal) << "| ";
			}
			cout << endl << "    ";
			for (int j = 0; j < _board.size(); j++)
			{
				cout << "___";
			}
			cout << endl;
		}
		cout << endl << endl;
	}
};

void processInput(Board & board)
{
	cout << ">>> ";
	string in;
	int x, y;
	cin >> in >> x >> y;
	char command = in[0];
	switch (command)
	{
	case 'r':
	case 'd':
		board.revealTile(x, y);
		break;
	case 'f':
		board.setFlagged(x, y, true);
		break;
	case 'u':
		board.setFlagged(x, y, false);
		break;
	default:
		cout << "Invalid command" << endl;
		break;
	}
}

int main()
{
	cout << "Enter Board Size: ";
	int Size;
	cin >> Size;
	Board  board(Size, 0.1);
	board.drawTextBased();
	while (board.checkWin() == 0)
	{
		processInput(board);
	}
	board.drawTextBased(true);
	if (board.checkWin() == 1)
	{
		cout << "You Win!" << endl;
	}
	if (board.checkWin() == -1)
	{
		cout << "Game Over" << endl;
	}
    return 0;
}

